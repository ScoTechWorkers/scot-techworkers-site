CREATE TABLE `quiz_submission` (
	`id` text PRIMARY KEY NOT NULL,
	`inserted_timestamp` integer DEFAULT CURRENT_TIMESTAMP NOT NULL,
	`requested_callback_email` text,
	`submitted_data` text NOT NULL,
	`requested_callback` integer,
	`other_info` text,
	FOREIGN KEY (`requested_callback_email`) REFERENCES `quiz_submitter`(`user_email`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
CREATE TABLE `quiz_submitter` (
	`user_email` text PRIMARY KEY NOT NULL,
	`submitter_name` text,
	`employer` text
);
