# Tech Workers Network Site

This is the site for the Scottish Tech Workers Network.

## Libraries and Tooling

| Task                           | Library                                                      |
| ------------------------------ | ------------------------------------------------------------ |
| Static site generation         | [Vike](https://vike.dev)                                     |
| Dev server                     | [Vite](https://vitejs.dev)                                   |
| CI/CD                          | [Woodpecker CI](https://woodpecker-ci.org)                   |
| Tests (not that there are any) | [Vitest](https://vitest.dev/)                                |
| Hosting                        | [Cloudflare Pages](https://developers.cloudflare.com/pages/) |
| Database                       | [Cloudflare D1](https://)                                    |
| Data validation                | [Zod](https://zod.dev)                                       |
| State store                    | [Pinia](https://pinia.vuejs.org)                             |
| ORM (ish)                      | [drizzle](https://orm.drizzle.team)                          |
| Dependency Management          | [renovate](https://renovatebot.com)                          |
| Icons                          | [Iconoir](https://iconoir.com/)                              |
| ID Generation                  | [Nano ID](https://zelark.github.io/nano-id-cc/)              |

## How to do Things

### Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Cloudflare pages

```sh
npx vite --port=4000
```

and then

```sh
npx wrangler pages dev --proxy 4000 --compatibility-date=2024-01-28
```

#### Deploy to Cloudflare Pages

```sh
npm run build
npx wrangler pages deploy "./dist/client/" --project-name scottish-techworkers-network
```
