import { fileURLToPath, URL } from 'node:url'

import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import md from 'unplugin-vue-markdown/vite'
import vike from 'vike/plugin'
import { defineConfig } from 'vite'
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    ViteImageOptimizer({
      svg: {
        plugins: ['preset-default'],
      },
    }),
    vike({ prerender: true }),
    vue({ include: [/\.vue$/, /\.md$/] }),
    vueJsx(),
    md({}),
  ],
  resolve: {
    alias: {
      '#root': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
})
