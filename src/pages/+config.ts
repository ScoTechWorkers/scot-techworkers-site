//import vikeVue from '#root/renderer/+config'
import type { Config } from 'vike/types'
import logoUrl from '../assets/logo-inner.svg'
import Head from '../layouts/HeadDefault.vue'
import Layout from '../layouts/LayoutDefault.vue'

// Default configs (can be overridden by pages)
export default {
  Layout,
  Head,
  // <title>
  title: 'Tech Workers Network - Scotland',
  // <meta name="description">
  description: 'Promoting Worker Power Through Unions in Tech in Scotland',
  // <link rel="icon" href="${favicon}" />
  favicon: logoUrl,

  meta: {
    piniaStore: { env: { client: true, server: true } },
  },
  // extends: vikeVue as Config,
} satisfies Config
