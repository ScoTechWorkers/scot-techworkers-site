# Resources

## Get in touch

The network is administered by [Maria Torres-Quevedo](mailto:maria.torres-quevedo@prospect.org.uk) who is available to talk all things union organisation in tech workplaces.

We're also available [on X, formerly known as Twitter](https://twitter.com/scotechwork).

## External Resources

### Unions

The Tech Workers Network is a project of [Prospect, the trade union for professionals](https://prospect.org.uk) who have a [Tech Workers](https://prospect.org.uk/tech-workers/) branch.

Other unions in the space are <abbr title="United Tech & Allied Workers">[UTAW](https://utaw.tech)</abbr> (a branch of the <abbr title="Communication Workers Union">CWU</abbr>), and [GMB Union](https://gmb.org.uk).

The <abbr title="Scottish Trades Union Congress">STUC</abbr> has a tool to find a relevant union.

If a union is already active in your workplace, we're happy to work with you to find ways to promote it!

[Organize.fyi](https://organize.fyi/) has a list of recent organising efforts in many sectors, including the [Tech Workers Coalition](https://techworkerscoalition.org).

### Government Support

The UK Government has [a page on unions, organising, and your rights](https://www.gov.uk/join-trade-union).

The Scottish Government is [implementing](https://www.gov.scot/publications/fair-work-action-plan-becoming-leading-fair-work-nation-2025/) the [Fair Work framework](https://www.fairworkconvention.scot/) for which unions are key as tools for [effective voice](https://www.fairworkconvention.scot/the-fair-work-framework/effective-voice/).
