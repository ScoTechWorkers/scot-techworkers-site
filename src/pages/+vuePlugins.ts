import type { VuePluginWithOptions } from '#root/renderer/+config'

import VueSidePanel from 'vue3-side-panel'
import 'vue3-side-panel/dist/vue3-side-panel.css'
import { piniaStore } from './+piniaStore'

// List of Vue plugins to install.
export default [
  {
    plugin: piniaStore,
    onClientRender(app, pageContext) {
      if (pageContext.initialStoreState)
        pageContext.config.piniaStore.state.value = pageContext.initialStoreState
    },
  },
  {
    plugin: VueSidePanel,
  },
] as VuePluginWithOptions[]
