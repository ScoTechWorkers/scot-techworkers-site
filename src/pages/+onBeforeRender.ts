// https://vike.dev/onBeforeRender

import type { Pinia } from 'pinia'
import type { OnBeforeRenderAsync, PageContextServer } from 'vike/types'

const onBeforeRender: OnBeforeRenderAsync = async function (pageContext: PageContextServer) {
  const initialStoreState = (pageContext.config.piniaStore as Pinia).state.value

  return {
    pageContext: {
      initialStoreState,
    },
  }
}

export { onBeforeRender }
