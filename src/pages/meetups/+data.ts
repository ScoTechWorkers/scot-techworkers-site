import ical from 'ical'
import type { PageContextServer } from 'vike/types'

export async function data(context: PageContextServer) {
  const url = context.config.calendarUrl
  if (!url) return
  const data = await fetchData(url)

  return { events: data }
}

async function fetchData(calendar_url: URL | string) {
  const response = await fetch(calendar_url, { redirect: 'follow' })

  if (response.ok) {
    const icalData = await response.text()
    return ical.parseICS(icalData)
  } else {
    throw new Error(await response.text())
  }
}
