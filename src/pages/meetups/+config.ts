import type { Config } from 'vike/types'

export default {
  meta: {
    calendarUrl: { env: { client: false, server: true } },
  },
} satisfies Config
