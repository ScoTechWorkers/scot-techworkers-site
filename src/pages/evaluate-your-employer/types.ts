export type SubmissionType = {
  requestCallback: boolean
  name?: string
  email?: string
  employer?: string
  otherInfo?: string
  seniority?: string
}
