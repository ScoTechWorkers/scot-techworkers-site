declare module 'ical' {
  declare type ICSEvent = {
    type: 'VEVENT'
    params: any[]
    uid: string
    sequence: `${number}`
    dtstamp: string
    start: string
    end: string
    summary: string
    location: string
    description: string

    [key: string]: {
      params: Record<string, string>
      val: string
    }?
  }
  declare function parseICS(input: string): Record<ICSEvent['uid'], ICSEvent>
}
