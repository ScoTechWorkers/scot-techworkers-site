import { sql } from 'drizzle-orm'
import { integer, sqliteTable, text } from 'drizzle-orm/sqlite-core'

export const quizSubmitter = sqliteTable('quiz_submitter', {
  email: text('user_email', { mode: 'text' }).primaryKey(),
  name: text('submitter_name'),
})

export const quizSubmission = sqliteTable('quiz_submission', {
  id: text('id').primaryKey(),
  insertedTimestamp: integer('inserted_timestamp', { mode: 'timestamp' })
    .notNull()
    .default(sql`CURRENT_TIMESTAMP`),
  submitterEmail: text('requested_callback_email').references(() => quizSubmitter.email, {
    onDelete: 'no action',
  }),
  submittedData: text('submitted_data', { mode: 'json' }).notNull(),
  requestedCallback: integer('requested_callback', { mode: 'boolean' }),
  employer: text('employer'),
  seniority: text('seniority', { mode: 'text', length: 255 }).default('unknown'),
  otherInfo: text('other_info'),
})
