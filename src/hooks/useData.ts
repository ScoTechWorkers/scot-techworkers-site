// https://vike.dev/useData

import { usePageContext } from '#root/hooks/usePageContext'
import { computed, type ComputedRef } from 'vue'

/** https://vike.dev/useData */
export function useData<Data>(): ComputedRef<Data> {
  const data = computed(() => (usePageContext() as { data: Data }).data)
  return data
}
