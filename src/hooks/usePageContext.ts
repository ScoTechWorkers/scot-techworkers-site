// Hook `usePageContext()` to make `pageContext` available from any Vue component.
// See https://vike.dev/pageContext-anywhere

import type { PageContext } from 'vike/types'
import type { App } from 'vue'
import { inject } from 'vue'
export { setPageContext, usePageContext }

const key = '__vike-vue__bcc79e46-5797-40d8-9cec-e9daf9c62ce8'

function usePageContext() {
  const pageContext = inject<PageContext>(key)
  return pageContext!
}

function setPageContext(app: App, pageContext: PageContext) {
  app.provide(key, pageContext)
}

declare global {
  namespace Vike {
    interface PageContext {
      config: {
        calendarUrl?: string
      }
    }
  }
}
