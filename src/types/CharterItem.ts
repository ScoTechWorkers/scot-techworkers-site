export type CharterItem = {
  id: string
  section: string
  subsection: string
  items: string[]
}
