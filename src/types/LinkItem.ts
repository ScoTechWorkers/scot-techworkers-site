import type { Component } from 'vue'

export type LinkItem = {
  label: string
  url: string | URL
  position?: 'left' | 'right'
  icon?: Component
}
