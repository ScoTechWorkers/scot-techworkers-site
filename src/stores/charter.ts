import type { CharterItem } from '#root/types/CharterItem'
import { defineStore } from 'pinia'

export const useCharterStore = defineStore('charter', {
  state: () => ({
    items: [
      {
        id: 'pay-transparency',
        section: 'Transparency in Work',
        subsection: 'Transparent pay and responsibilities',
        items: [
          'Defined pay grades with clear objectives, responsibilities, and paths for progression.',
        ],
      },
      {
        id: 'security',
        section: 'Transparency in Work',
        subsection: 'Security against redundancy',
        items: [
          'Transparency on funding and financial health.',
          'Offering permanent contracts wherever possible.',
        ],
      },
      {
        id: 'non-compete',
        section: 'Transparency in Work',
        subsection: 'Sensible non-competes',
        items: ["Non-compete clauses that don't lock employees out of future jobs."],
      },
      {
        id: 'worklife-balance',
        section: 'Quality and partnership at work',
        subsection: 'Right to switch off',
        items: [
          'No sign out of the Working Time Directive.',
          'Collaborative approach with workers to establish rules around what on call, flexible work, and fair hours should entail.',
        ],
      },
      {
        id: 'ip-rights',
        section: 'Quality and partnership at work',
        subsection: 'Right to Intellectual Property',
        items: [
          'By default, workers should be allowed to own their personal code and re-use it.',
          'The employer forsakes rights to any project done outside of contracted work.',
        ],
      },
      {
        id: 'flexible-work',
        section: 'Quality and partnership at work',
        subsection: 'Flexible working hours',
        items: [
          'Options available for remote work and part-time work.',
          'This should include progression objectives that do not exclude non-traditional working.',
        ],
      },
      {
        id: 'edi',
        section: 'Tech For Good',
        subsection: 'Equality, Diversity and Inclusion',
        items: [
          `Tracking pay gaps and committing to closing them`,
          `Policy and training around inclusion of the protected characteristics of the Equality Act 2010.`,
          `Dropping 'grade requirements' and university degrees as 'essential' and taking CVs on the merit of experience and education.`,
        ],
      },
      {
        id: 'ethics',
        section: 'Tech For Good',
        subsection: 'Ethical Protections',
        items: [
          `Workers must have the right to withhold their labour from projects that are of widespread ethical concern.`,
          `Whistleblower protections.`,
        ],
      },
      {
        id: 'sustainability',
        section: 'Tech For Good',
        subsection: 'Sustainability',
        items: [
          `Divest from fossil fuels, with a commitment to a Just Transition`,
          `Sustainability central to the greatest extent possible throughout the supply chain.`,
        ],
      },
    ] as CharterItem[],
  }),
  getters: {
    itemsAsMap: (state) =>
      state.items.reduce((acc, v) => ({ ...acc, [v.id]: v }), {} as Record<string, CharterItem>),
  },
})
