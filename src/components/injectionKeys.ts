import type { InjectionKey } from 'vue'

export const CardGridParent = Symbol('CardGridParent') as InjectionKey<boolean>
