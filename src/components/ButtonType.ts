export enum ButtonType {
  Primary = 'primary',
  Secondary = 'secondary',
  Action = 'action',
}
