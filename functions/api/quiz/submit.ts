import { quizSubmission, quizSubmitter } from '#root/db/schema'
import { drizzle } from 'drizzle-orm/d1'
import { nanoid } from 'nanoid'
import { ZodError, z } from 'zod'

interface Env {
  DB: D1Database
}

export const quizResponseSchema = z.object({
  data: z.record(z.number().positive().lte(5).gte(0)),
  requestCallback: z.boolean().default(false),
  name: z.string().optional(),
  email: z.string().email().optional(),
  employer: z.string().max(255),
  seniority: z.string().max(255).default('unknown'),
  otherInfo: z.string().optional(),
})

export const onRequestPost: PagesFunction<Env> = async (context) => {
  try {
    const requestJson = quizResponseSchema.parse(await context.request.json())
    const db = drizzle(context.env.DB)

    if (requestJson.email && (requestJson.employer || requestJson.name)) {
      await db
        .insert(quizSubmitter)
        .values({
          email: requestJson.email,
          name: requestJson.name,
        })
        .onConflictDoNothing()
    }

    await db.insert(quizSubmission).values({
      id: nanoid(),
      submittedData: requestJson.data,
      otherInfo: requestJson.otherInfo,
      submitterEmail: requestJson.email,
      requestedCallback: requestJson.requestCallback,
      employer: requestJson.employer,
      seniority: requestJson.seniority,
    })

    return new Response('{}', { status: 200 })
  } catch (e) {
    if (e instanceof ZodError) {
      return new Response(JSON.stringify(e.format()), { status: 400 })
    }

    console.error(e)
    return new Response('An error occurred', { status: 500 })
  }
}
